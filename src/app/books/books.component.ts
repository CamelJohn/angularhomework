import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})

export class BooksComponent implements OnInit {

  @Output() panelOpenState = false;
  books = [
    {title: 'Alice in Wonderland', author: 'Lewis Carrol', description: 'Down the rabbit hole'},
    {title: 'War and Peace', author: 'Leo Tolstoy', description: 'We are all dying'},
    {title: 'The Magic Mountain', author: 'Thomas Mann', description: 'Another trippy children\'s book'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
